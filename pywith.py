# with open('myfile.txt','r') as f:   #if u not write any access mode then by default it is read mode
#     data=f.read()
#     print(data)

with open('myfile.txt') as f:   #if u not write any access mode then by default it is read mode
    data=f.write()   # so it gives an error
    print(data)    