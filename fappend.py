# file=open('myfile.txt','w')

# file.write('SomeText Awake!\n and stop')

# file.close()

file=open('myfile.txt','a')   # x is used to create file
# print(file.tell())  # tell tells us about pointer location

# file.write('SomeText')
# print(file.tell())
print(file.readline())
print(file.readlines())

file.close()